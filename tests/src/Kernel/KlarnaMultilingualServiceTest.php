<?php

namespace Drupal\Tests\klarna_multilingual\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\klarna_multilingual\EventSubscriber\KlarnaMultilingualSubscriber;

/**
 * Test to ensure 'klarna_multilingual_subscriber' service is reachable.
 *
 * @group klarna_multilingual
 * @group examples
 *
 * @ingroup klarna_multilingual
 */
class KlarnaMultilingualServiceTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['klarna_multilingual'];

  /**
   * Test for existence of 'klarna_multilingual_subscriber' service.
   */
  public function testKlarnaMultilingualService() {
    $subscriber = $this->container->get('klarna_multilingual_subscriber');
    $this->assertInstanceOf(KlarnaMultilingualSubscriber::class, $subscriber);
  }

}
