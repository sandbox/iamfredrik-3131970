<?php

namespace Drupal\klarna_multilingual\EventSubscriber;

use Drupal\commerce_klarna_checkout\Event\KlarnaCheckoutEvents;
use Drupal\commerce_klarna_checkout\Event\OrderRequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to IncidentEvents::NEW_REPORT events and react to new reports.
 *
 * In this example we subscribe to all IncidentEvents::NEW_REPORT events and
 * point to two different methods to execute when the event is triggered. In
 * each method we have some custom logic that determines if we want to react to
 * the event by examining the event object, and the displaying a message to the
 * user indicating whether or not that method reacted to the event.
 *
 * By convention, classes subscribing to an event live in the
 * Drupal/{module_name}/EventSubscriber namespace.
 *
 * @ingroup events_example
 */
class KlarnaMultilingualSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KlarnaCheckoutEvents::CREATE_ORDER_REQUEST => 'createOrder',
      KlarnaCheckoutEvents::UPDATE_ORDER_REQUEST => 'updateOrder',
    ];
  }

  /**
   * React to a order being created.
   *
   * @param \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent $event
   * 
   */
  public function createOrder(OrderRequestEvent $event) {  
    $data = $event->getRequestData();
    $data['locale'] = $this->getLanguage($data['locale']);
    $event->setRequestData($data);
  }

  /**
   * React to a order being updated.
   *
   * @param \Drupal\commerce_klarna_checkout\Event\OrderRequestEvent $event
   * 
   */
  public function updateOrder(OrderRequestEvent $event) {
    $data = $event->getRequestData();
    $data['locale'] = $this->getLanguage($data['locale']);
    $event->setRequestData($data);
  }

  public function getLanguage($locale) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

    switch (true) {
      case (($locale == 'sv-fi' || $locale == 'fi-fi') && $language == 'sv'):
        $locale = 'sv-fi';
        break;
      case (($locale == 'sv-fi' || $locale == 'fi-fi') && $language == 'fi'):
        $locale = 'fi-fi';
        break;
      default:
        $locale = $locale;
        $break;
    }

    return $locale;
  }

}
